<?php

$da= new DataAccess($link);
$recentPosts = $da->get_recent_post();
?>
<div class="footer">
    <div class="container">
        <div class="footer-wrapper">
            <div class="row">
                <!-- Footer Col. -->
                <div class="col-md-3 col-sm-3 footer-col">
                    <div class="footer-content">
                        <div class="footer-content-logo">
                            <h1 style = "color:white">Jerry Vongkhamsay</h1>
                        </div>
                        <div class="footer-content-text">
                            <p>Lorem ipsum dolor sit amet nec, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor.
                            </p>
                            <p>Lorem ipsum dolor sit amet nec, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor.
                            </p>
                        </div>
                    </div>
                </div>
                <!-- //Footer Col.// -->
                <!-- Footer Col. -->
                <div class="col-md-3 col-sm-3 footer-col">
                    
                </div>
                <!-- //Footer Col.// -->
                <!-- Footer Col. -->
                <div class="col-md-3 col-sm-3 footer-col">
                    <div class="footer-title">
                        Recent Blogs
                    </div>
                    <div class="footer-content">
                        <ul class="footer-category-list">
                            <?php
                            foreach($recentPosts as $recentPost){ 
          
                                                  ?>      
                                                <li>

                                                        <a href="" class="posts-list-title"> <?php echo("<a href=\"blogSingle.php?post_id={$recentPost['post_id']}\">{$recentPost['post_title']}</a>"); ?></a>

                                                </li>
                                                <?php
                                                } 
                                                        ?>
                           
                        </ul>
                    </div>
                </div>
                <!-- //Footer Col.// -->
                <!-- Footer Col. -->
                <div class="col-md-3 col-sm-3 footer-col">
                    <div class="footer-title">
                        Twitter
                    </div>
                    <div class="footer-content">
                        <div class="flickr_badge_wrapper">
                          
<a class="twitter-timeline" href="https://twitter.com/JVongkhamsay" data-widget-id="593199401742372864">Tweets by @JVongkhamsay</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
                        </div>
                        <!-- //Footer Col.// -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 center-text">
                    <div class="copyright-text">&copy; 2015 Jerry Vongkhamsay | <a href="/private_policy.php">Private Policy</a></a></div>
                </div>
            </div>
        </div>
    </div>
</div>