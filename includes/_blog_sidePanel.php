<?php
$recentPosts = $da->get_recent_post();
?>

<div class="sidebar">


                                    <!-- Sidebar Block -->
                                    <div class="sidebar-block">
                                        <div class="sidebar-content tags blog-search">
                                            <form action="#" method="post">
                                                <div class="input-group">
                                                    <input type="text" name="q" class="form-control blog-search-input text-input" placeholder="Search.."/>
                                                    <span class="input-group-addon">
                                                        <button class="blog-search-button icon-search ">
                                                        </button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Sidebar Block -->


                                    <!-- Sidebar Block -->
                                    <div class="sidebar-block">
                                        <h3 class="h3-sidebar-title sidebar-title">
                                            Categories
                                        </h3>

                                        <div class="sidebar-content tags">
                                       
                                                    
                                                          <?php 
                                                     
                                                foreach($categories as $cat){ 
                                                 
                                                        echo("<a href=\"blogs_by_category.php?category_id={$cat['category_id']}\">{$cat['category_name']}</a>");
                                                        } 
                                             
                        
                       
             
                        
                        ?>

                                        </div>
                                    </div>
                                    <!-- Sidebar Block -->


                                    <!-- Sidebar Block -->
                                    <div class="sidebar-block">
                                        <h3 class="h3-sidebar-title sidebar-title">
                                            recent posts
                                        </h3>

                                        <div class="sidebar-content">
                                            <ul class="posts-list">
                                                
                                                    <?php 
                                                     
                                                foreach($recentPosts as $recentPost){ 
                                                  $time=strtotime($recentPost[ 'post_date']);
                                                  $post_date=date( "m/d/y", $time); echo( "<tr>"); 
                                                       
                                                  ?>      
                                                <li>
                                                 
                                                
                                                        <a href="" class="posts-list-title"> <?php echo("<a href=\"blogSingle.php?post_id={$recentPost['post_id']}\">{$recentPost['post_title']}</a>"); ?></a>
                                                        <span class="posts-list-meta">
                                                            <br>
                                                           <?php
                                                           echo $post_date;
                                                           ?>
                                                        </span>
                                                  
                                                </li>
                                                <?php
                                                } 
                                                        ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Sidebar Block -->
                                </div>