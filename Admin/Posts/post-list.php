<?php
include '../../includes/_header.php'; 
include( "../../DataUtil/common.inc.php"); 
include( "../../DataUtil/DataAccess.inc.php"); 

$screenName ="";
if(isset($_SESSION['screenName'])){
    $screenName = $_SESSION['screenName'];
}
if($screenName){ 
    
    $da=new DataAccess($link); 
    $post=$da->get_post(); 
    $getCategory= $da->get_categories(); 
    $getImageList = $da->get_images();
  
    // output data of each row 
    //Get post ID 
    if(isset($_GET['post_id'])){ 
        $post_id = $_GET['post_id'];
        $get_post=$da->get_post_by_id("$post_id");
    } 
    $findPost = "";
    if(isset($_POST['btnSearchPost'])){
        if(isset($_POST['searchPost'])){
        $searchPosts = $_POST['searchPost'];    
        
        $findPost = $da->get_post_by_postContent($searchPosts);
       
        }
    }
        
        if(isset($_POST['btnSubmit'])){ 
            //Validations
//Validate post title 
            $postTitle = $_POST['post_title']; 
            //check to see if updating an exsiting category //or inserting a new one // 
            $new_category_name = ""; 
            $new_post_title = htmlentities($_POST['post_title']); 
            $new_post_date = htmlentities($_POST['post_date']);
            $new_category_id = htmlentities($_POST['category_id']);
            $new_post_active = htmlentities($_POST['post_active']);
            $new_post_content = strip_tags($_POST['post_content'] ,'<img>, <p>');
            $new_post_raw_content = htmlentities($_POST['post_raw_content']);
            $new_post_description = htmlentities($_POST['post_description']); 
            $da->insert_post( $new_post_title, $new_post_date,$new_category_id, $new_post_active, $new_post_content, $new_post_raw_content, $new_post_description); 
            echo 'It submitted.';
             ?>
    <meta http-equiv="refresh" content="0">
    <?php
// 
} 
        if(isset($_POST['btnSubmitEdit'])){
            //Validations
            echo 'It submitted.';
            //check to see if updating an exsiting category
            //or inserting a new one
            // $new_category_name = "";
            $new_post_title = htmlentities($_POST['post_title']);
            $new_post_active = htmlentities($_POST['post_active']);
            $new_post_content = strip_tags($_POST['post_content'], '<img>, <p>');
            $new_post_raw_content = htmlentities($_POST['post_raw_content']);
            $new_post_description = htmlentities($_POST['post_description']);
   
            $da->update_posts($post_id, $new_post_title, $new_post_active, $new_post_content, $new_post_raw_content, $new_post_description);
             ?>
    <meta http-equiv="refresh" content="0">
    <?php
        }
?>



<div class="tab-container">
    <ul class="etabs">

        <li class="tab">
            <a href="#tab1"><i class=icon-list></i>Post List</a>
        </li>


        <li class="tab">
            <a href="#tab2"><i class=icon-file-add></i>Add Post</a>
        </li>


        <li class="tab">
            <a href="#tab3"><i class=icon-arrow-up></i>Update Post</a>
        </li>
        <li class="tab">
            <a href="#tab4"><i class=icon-search></i>Search Posts</a>
        </li>

    </ul>

    <div class="tabs-content">
        <div id="tab1">
            <table class="table">
                <thead>
                    <th>Post Title</th>
                    <th>Post Date</th>
                    <th>Content</th>
                    <th>Post Active</th>
                    <th>Post Description</th>
                    <th></th>

                </thead>
                <tbody>
                    <?php 
                    foreach($post as $post){ 
                        $time=strtotime($post[ 'post_date']);
                        $post_date=date( "m/d/y", $time); echo( "<tr>"); 
                        echo( "<td>{$post['post_title']}</td>"); 
                        echo( "<td>$post_date</td>"); 
                        echo( "<td>{$post['post_content']}</td>"); 
                        echo( "<td>{$post['post_active']}</td>"); 
                        echo( "<td>{$post['post_description']}</td>"); 
                        echo("<td><a href=\"post-list.php?post_id={$post[ 'post_id']}#tab3\">EDIT</a></td>"); echo( "</tr>");
                        } 
                        ?>
                </tbody>
            </table>
        </div>
        <div id="tab2">
            <form method="POST" action="">
                Post Title:
                <br>
                <input type="text" style="width:750px" name="post_title" placeholder="Post title" maxlength="100" value="" required>
                
                <br> Category:
                <br>
                <select name="category_id" required>
                    <?php foreach($getCategory as $cat){ echo( "<option value = '{$cat['category_id']}'>{$cat['category_name']}</option>"); } ?>
                </select>
                <br> Post Date:
                <br>
                <input type="Date" name="post_date" value="" required>
                <br> Post Description:
                <br>
                <input type="text" name="post_description" style="width:750px" placeholder="Post Desctiption" maxlength="160" value="" required>
                <br> Post Active:
                <select name="post_active">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <br> Post Content:
                <br>
                <textarea name="post_content" id="post_content_add" style="width:750px" rows="4" cols="50" placeholder="Post Content" value="" required></textarea><br>
                Insert Image:<select name="imageAdd" id="imageAdd" required>
                    <?php foreach($getImageList as $getImages){
                    echo( "<option value = '{$getImages['image_filename']}'>{$getImages['image_filename']}</option>"); } ?>
                    
                </select>
                <input type="button" name="btnInsertImgAdd" id = "btnInsertImgAdd" value="Insert Image">
                <br> Post raw content:
                <br>
                <textarea style="width:750px" name="post_raw_content" rows="4" cols="50" placeholder="Post Raw Content" value="" required></textarea>
                <br>


                <input type="submit" name="btnSubmit" value="Submit">
            </form>
        </div>
        <div id="tab3">
            <?php if(isset($_GET[ 'post_id'])){ 
            ?>

            <form method="POST" action="">
                Post Title:
                <br>
                <input type="text" name="post_title" placeholder="Post title" maxlength="100" style="width:750px" value="<?php echo $get_post[0]['post_title'] ?>" required>
                <br> Post Description:
                <br>
                <input type="text" name="post_description" placeholder="Post Desctiption" style="width:750px" maxlength="160" value="<?php echo $get_post[0]['post_description'] ?>" required>
                <br> Post Active:
                <select name="post_active">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <br> Post Content:
                <br>
                <textarea cols="50" rows="4" style="width:750px" name="post_content" id="post_content" placeholder="Post Content" value="" required><?php echo $get_post[0][ 'post_content'] ?></textarea><br>
                Insert Image:<select name="selImage" id="selImage" required>
                    <?php foreach($getImageList as $getImages){
                    echo( "<option value = '{$getImages['image_filename']}'>{$getImages['image_filename']}</option>"); } ?>
                    
                </select>

                <input type="button" name="btnInsertImg" id = "btnInsertImg" value="Insert Image">
                <br> Post raw content:
                <br>
                <textarea name="post_raw_content" placeholder="Post Raw Content" rows="4" cols="50" style="width:750px" value="" required><?php echo $get_post[0][ 'post_raw_content'] ?></textarea>
                <br>


                <input type="submit" name="btnSubmitEdit" value="Submit">
            </form>
            <?php
            }else{
                echo 'Please select a post to edit!';
            }
       ?>
        </div>

        <div id="tab4">
            <form method="POST" action="">
                <input type="text" style="width:300px" name="searchPost" placeholder="Search..." value="" required>
                <input type="submit" name="btnSearchPost" value="Search">

            </form>


            <table class="table">
                <thead>
                    <th>Post Title</th>
                    <th>Post Date</th>
                    <th>Content</th>
                    <th></th>

                </thead>
                <tbody>
                    
                    <?php 
                    if(isset($_POST['btnSearchPost'])){
                    foreach($findPost as $findPost){ 
                        $time=strtotime($findPost[ 'post_date']);
                        $post_date=date( "m/d/y", $time); echo( "<tr>"); 
                        echo( "<td>{$findPost['post_title']}</td>"); 
                        echo( "<td>$post_date</td>"); 
                        echo( "<td>{$findPost['post_content']}</td>"); 
                        echo("<td><a href=\"post-list.php?post_id={$findPost[ 'post_id']}#tab3\">EDIT</a></td>"); 
                        echo( "</tr>");
                        } 
                        }
                        ?>
                </tbody>
            </table>

        </div>
    </div>

</div>
<?php 
            
        }else{
          
            echo "you are not an admin"; 
            
        }
            include '../../includes/_footer.php';
            
?>

<script>

$(function(){
  $("#btnInsertImg").click(function(){
    var imgDir = "/images/"; 
    var imgName = $("#selImage").val();
    if(imgName == ""){
        
      alert("You must choose an image to insert");
      
      return;
    }

    var imgPath = imgDir + imgName;

    var imgTag = '<img src="' + imgPath + '"  width="650" height="350"/>'; 


    var caretPos = document.getElementById("post_content").selectionStart;

    var textAreaTxt = jQuery("#post_content").val();

    jQuery("#post_content").val(textAreaTxt.substring(0, caretPos) + imgTag + textAreaTxt.substring(caretPos) );


  });


});

</script>
<script>

$(function(){

  

  $("#btnInsertImgAdd").click(function(){
    var imgDir = "/images/"; 
    var imgName = $("#imageAdd").val();
    if(imgName == ""){
        
      alert("You must choose an image to insert");
      
      return;
    }

    var imgPath = imgDir + imgName;

    var imgTag = '<img src="' + imgPath + '"  width="650" height="350"/>'; 


    var caretPos = document.getElementById("post_content_add").selectionStart;

    var textAreaTxt = jQuery("#post_content_add").val();

    jQuery("#post_content_add").val(textAreaTxt.substring(0, caretPos) + imgTag + textAreaTxt.substring(caretPos) );


  });


});

</script>