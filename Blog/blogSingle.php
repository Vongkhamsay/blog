<?php
include '../includes/_header.php';
include( "../DataUtil/common.inc.php"); 
include( "../DataUtil/DataAccess.inc.php");

$da=new DataAccess($link);
$categories=$da->get_categories();

//if(isset($_GET['post_id'])){ 
       $post_id = $_GET['post_id'];
       $get_post= $da->get_post_by_id("$post_id");
  //  } 
?>
<div class="top-title-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="page-info">
                                    <h1 class="h1-page-title">Blog</h1>

                                    <h2 class="h2-page-desc">
                                        Single Post Page
                                    </h2>

                                    <!-- BreadCrumb -->
                                    <div class="breadcrumb-container">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="">Home</a>
                                            </li>
                                            <li class="active">Blog</li>
                                        </ol>
                                    </div>
                                    <!-- BreadCrumb -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--.top wrapper end -->

            <div class="loading-container">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>


            <div class="content-wrapper hide-until-loading"><div class="body-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-sm-9">


                                <!-- Blog Post -->
                                <div class="blog-post">

                                    <div class="blog-post-type">
                                        <i class="icon-pen"> </i>
                                    </div>

                                    <div class="blog-span">
                                        <div class="">
                                            <h2>
                                            <?php echo $get_post[0]['post_title'] ?>
                                        </h2>

                                        </div>
                       
                                        

                                        <div class="blog-post-body">
                                        
                                            <?php echo $get_post[0]['post_content'] ?>
                                        </div>

                                      <div class="blog-post-details">
                                          <?php
                                           foreach($get_post as $post){ 
                                                $time=strtotime($post[ 'post_date']);
                                                $post_date=date( "m/d/y", $time); echo( "<tr>"); 
                                                $categoryID = $post['category_id'];
                                                $categoryName= $da->get_categories_by_id($categoryID);
                                           }
                                          ?>

                                            <!-- Date -->
                                            <div class="blog-post-details-item blog-post-details-item-left icon-calendar">
                                            <?php
                                            echo $post_date;
                                            ?>
                                            <?php
                                            echo('
                                            </div>

                                            <div class="blog-post-details-item blog-post-details-item-left icon-user">
                                                <a href="">
                                                    Admin
                                                </a>
                                            </div>
                                            <!-- //Author Name// -->


                                            <!-- Tags -->
                                            <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags icon-files">
                                                  ')?>
                                            
                                            <?php
                                                echo $categoryName[0]['category_name'];
                                                ?>
                                               
                                            </div>
                                            <!-- //Tags// -->

                                        </div>
                                        <div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'discoveranime';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

                                    </div>

                                </div>
                                <!-- //Blog Post// -->


                           

                            </div>


                            <div class="col-md-3 col-sm-3">
                                <?php include '../includes/_blog_sidePanel.php'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--.content-wrapper end -->
                                    <?php
          
            include '../includes/_footer.php';
            ?>