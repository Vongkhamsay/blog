<?php
include '../includes/_header.php';
include( "../DataUtil/common.inc.php"); 
include( "../DataUtil/DataAccess.inc.php");

//GET CATEGORIES
$da=new DataAccess($link);
$categories=$da->get_categories();
$post=$da->get_post_Order_By_Date(); 

$category_id= "";
$posts_by_category_id = "";
if(isset($_GET{'category_id'})){
    

$category_id = $_GET['category_id'];

$posts_by_category_id = $da->get_post_by_category_id($category_id);
}
?>


<div class="loading-container">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>


            <div class="content-wrapper hide-until-loading"><div class="top-title-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="page-info">
                                    <h1 class="h1-page-title">Blogs</h1>

                                    <h2 class="h2-page-desc">
                                        What We have Done Lately
                                    </h2>

                                    <!-- BreadCrumb -->
                                    <div class="breadcrumb-container">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="">Home</a>
                                            </li>
                                            <li class="active">Blog</li>
                                        </ol>
                                    </div>
                                    <!-- BreadCrumb -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-sm-9">

                                 <?php 
                    if(isset($_GET['category_id'])){
                    foreach($posts_by_category_id as $post){ 
                        $time=strtotime($post[ 'post_date']);
                        $post_date=date( "m/d/y", $time); echo( "<tr>"); 
                        $categoryID = $post['category_id'];
                        $categoryName= $da->get_categories_by_id($categoryID);
                        ?>
                                <!-- Blog Post -->
                                <div class="blog-post">

                                    <div class="blog-post-type">
                                        <i class="icon-pen"> </i>
                                    </div>

                                    <div class="blog-span">
                                       
                                        <h2>
                                          <?php  echo("<a href=\"blogSingle.php?post_id={$post['post_id']}\">"); ?>
                                  
                                            
                                            <?php
                                                echo $post['post_title'];
                                                ?>
                                           
                                            
                                            </a>
                                        </h2>

                                        <div class="blog-post-body">
                                        
                                           
                                            <?php
                                            echo $post['post_description'];
                                            ?>
                                     
                                    
                                            </div>

                                        <div class="blog-post-details">

                                            <!-- Date -->
                                            <div class="blog-post-details-item blog-post-details-item-left icon-calendar">
                              
                                            <?php
                                            echo $post_date;
                                            ?>
                                         
                                           
                                            </div>

                                            <div class="blog-post-details-item blog-post-details-item-left icon-user">
                                                <a href="">
                                                    Admin
                                                </a>
                                            </div>
                                            <!-- //Author Name// -->


                                            <!-- Tags -->
                                            <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags icon-files">
                                          
                                          
                                           
                                            <?php
                                                 echo $categoryName[0]['category_name']; 
                                                ?>
                                             
                                                
                                              
                                            </div>
                                            <!-- //Tags// -->

                                        
                                        


                                            <!-- Read More -->
                                            <div class="blog-post-details-item blog-post-details-item-right">
                                                <?php  echo("<a href=\"blogSingle.php?post_id={$post['post_id']}\">"); ?>
                                                    read more <i class="fa fa-chevron-right"></i>
                                                </a>
                                            </div>
                                            <!-- //Read More// -->

                                        </div>
                                    </div>
                                </div>
                                <!-- Blog Post -->
                         <?php
                    }
                    }
                                
                                
                                ?>

                            </div>


                            <div class="col-md-3 col-sm-3">
                                <?php
                                include '../includes/_blog_sidePanel.php';
                                ?>
                            </div>


                        </div>
                    </div>
                </div>
            </div><!--.content-wrapper end -->
                        <?php
          
            include '../includes/_footer.php';
            ?>