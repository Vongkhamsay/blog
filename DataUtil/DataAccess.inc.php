<?php
/**
* Handles all calls to the database for the entire application
*/
class DataAccess{
	
	/**
	* @var resource $link 	The connection to the database
	*/
	private $link;
	
	/**
	* Constructor method
	* 
	* @param resource $link 	Sets the $link property
	*/
	function __construct($link){
		$this->link = $link;
	}

	/**
	* Authenticates a user for accessing the control panel
	* 
	* @param string email
	* @param string password
	* 
	* @return assoc array if login is authenticated, returns false if authentication fails
	*/
	function login($email, $password){
	
		$email = mysqli_real_escape_string($this->link, $email);
		$password = mysqli_real_escape_string($this->link, $password);

		$qStr = "SELECT user_display_name FROM tblUsers WHERE user_email = '$email' AND user_password = '$password' AND user_active = 'yes'";
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		$num_rows = $result->num_rows;
		
		if($num_rows == 1){
			// NOTE: not the diff between msqli_fetch_array() and mysqli_fetch_assoc()
			// return mysqli_fetch_array($result);
			return mysqli_fetch_assoc($result);
		}elseif($num_rows > 1){
			$this->handle_error("Duplicate email and passwords in DB!");
			return false;
		}else{
			return false;
		}
	}

	function handle_error($err_msg){
		if(DEBUG_MODE){
			die($err_msg);
		}else{
			//TODO: handle errors in production
		}
	}
	
	function get_categories(){
		$qStr ="SELECT category_id, category_name FROM tblCategories ORDER BY category_name";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$categories = array();
		while($row = mysqli_fetch_assoc($result)){
			$categories[] =$row;
		}
		
		return $categories;
	}
	
		
	function get_images(){
		$qStr ="SELECT image_id, image_filename FROM tblImages ORDER BY image_filename";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$image = array();
		while($row = mysqli_fetch_assoc($result)){
			$image[] =$row;
		}
		
		return $image;
	}
	
	
	function get_post(){
		$qStr ="SELECT * FROM tblPost ORDER BY post_title";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$post = array();
		while($row = mysqli_fetch_assoc($result)){
			$post[] =$row;
		}
		
		return $post;
	}
	
	
	function insert_category($categories){
		$qStr = "INSERT INTO tblCategories(category_id, category_name) VALUES (NULL, '$categories')";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		

	
		}
		
	function insert_image($image){
		$qStr = "INSERT INTO tblImages(image_id, image_filename) VALUES (NULL, '$image')";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
	}
	
	function insert_post($title, $date, $category_id, $active, $content, $raw_content, $description){
		$qStr = "INSERT INTO tblPost(post_id, post_date, post_title, post_active, category_id, post_content,post_raw_content, post_description) VALUES (NULL, '$date', '$title', '$active', '$category_id', '$content', '$raw_content', '$description')";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
	}

	function get_categories_by_id($category_id){
		$qStr = "SELECT category_name FROM tblCategories WHERE category_id= '$category_id'";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$row = mysqli_fetch_assoc($result);
		
		$category[] = $row;
		
	
		return $category;
}
	function get_post_by_id($post_id){
		$qStr = "SELECT * FROM tblPost WHERE post_id= '$post_id'";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
	

		$row= mysqli_fetch_assoc($result);
		
		$name[] = $row;
		return $name;
}
	function update_category($category, $category_id){
		$qStr = "UPDATE tblCategories SET category_id = category_id, category_name = '$category' WHERE category_id = '$category_id'";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
	}
	
	function update_posts(  $post_id, $title, $post_active, $post_content, $post_raw_content, $post_description){
			$qStr = "UPDATE `tblPost` SET `post_id`=post_id,`post_date`=post_date,`post_title`='$title',`post_active`='$post_active',`category_id`=category_id,`post_content`='$post_content',`post_raw_content`='$post_raw_content',`post_description`='$post_description' WHERE post_id = '$post_id'";
			$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
			$posts = array();
			while($row = mysqli_fetch_assoc($result)){
			$posts[] =$row;
			}
			return $posts;
	}
	
	
	function get_categories_by_categoryName($category){
		$qStr ="SELECT category_id, category_name FROM tblCategories WHERE `category_name` LIKE '%$category%' ORDER BY category_name";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$categories = array();
		while($row = mysqli_fetch_assoc($result)){
			$categories[] =$row;
		}
		
		return $categories;
		
		
	
	}
	function get_post_by_postContent($postContent){
		$qStr ="SELECT post_id, post_title, post_date, post_content FROM tblPost WHERE `post_content` LIKE '%$postContent%' ORDER BY post_title";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$post = array();
		while($row = mysqli_fetch_assoc($result)){
			$post[] =$row;
		}
		
		return $post;
		
		
	
	}
	function get_post_Order_By_Date(){
		$qStr ="SELECT * FROM tblPost WHERE post_active = 'yes' ORDER BY post_date DESC";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$post = array();
		while($row = mysqli_fetch_assoc($result)){
			$post[] =$row;
		}
		
		return $post;
	}
	
	function get_post_by_category_id($category_id){
		$qStr ="SELECT * FROM tblPost WHERE category_id = '$category_id' AND post_active ='yes'";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$post = array();
		while($row = mysqli_fetch_assoc($result)){
			$post[] =$row;
		}
		
		return $post;
		
		
	
	}
	
	function get_recent_post(){
		$qStr ="SELECT * FROM tblPost WHERE post_active = 'yes' ORDER BY post_date DESC LIMIT 5";
		$result = mysqli_query($this->link,$qStr) or $this->handle_error(mysqli_error($this->link));
		$post = array();
		while($row = mysqli_fetch_assoc($result)){
			$post[] =$row;
		}
		
		return $post;
		
	}

}
// notice there is no closing php delimiter for files that are meant to be embedded