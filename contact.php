<?php
include 'includes/_header.php';
include( "DataUtil/common.inc.php"); 
include( "DataUtil/DataAccess.inc.php");

$name = "";
$email = "";
$subject = "";
$message = "";
if(isset($_POST['btnSubmit'])){
    $myEmail = "jvongkhamsay@hotmail.com";
    $name = $_POST['name'];
    $fromEmail = $_POST['email'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    
    mail($myEmail, $subject, $name . " Message:" . " ". $message, "From: " . $fromEmail);
    
    echo 'Your email has been sent!<br>';
    echo "To: " . $myEmail;
    echo "<br>From: " . $fromEmail;
    echo "<br>Subject " . $subject;
    echo "<br>Sent by: ". $name. " <br>Message: " . $message;


}
?>

            <div class="loading-container">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>


            <div class="content-wrapper hide-until-loading">





                <!-- Contact Map -->
                <div class="body-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-sm-8">
                                <div class="contact-body">
                                    <h3 class="h3-body-title">
                                        Leave A Message
                                    </h3>
                                    <p class="body_paragraph contact-paragraph">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                                    </p>

                                    <form class="form-wrapper" id="contact-form" method="post" role="form" novalidate>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-7">
                                                    <label for="name">
                                                        Name *
                                                    </label>
                                                    <input type="text" id="name" name="name" class="form-control" data-errmsg="Name is required."
                                                           minlength="2" placeholder="Your Name" required/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-7">
                                                    <label for="email">
                                                        Email *
                                                    </label>
                                                    <input type="text" id="email" name="email" class="form-control" data-errmsg="Email is required."
                                                           minlength="2" placeholder="Your Email" required/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-7">
                                                    <label for="message">
                                                        Subject
                                                    </label>
                                                    <input type="text" id="subject" name="subject" class="form-control"
                                                           placeholder="Your Subject"/>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <label for=message">Message *</label>
                                                    <textarea id="message" name="message" class="form-control" data-errmsg="Message is required."
                                                              placeholder="Your Message" rows="3" required></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 offset2">
                                                <input type="submit" name = "btnSubmit" value="Submit" class="btn btn-lg" />
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-md-offset-1 col-sm-offset-1">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--.content-wrapper end -->
            
            <?php
include 'includes/_footer.php';
?>
    
